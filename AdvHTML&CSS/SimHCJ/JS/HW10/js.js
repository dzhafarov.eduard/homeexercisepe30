"use strict";

let password = document.getElementById("password");
let confirmPassword = document.getElementById("confirm-password");
let btn = document.querySelector('.btn');
let faEyeSlash = document.querySelector ('.fa-eye-slash');
let iconPasswordConfirm = document.querySelector('.icon-password-confirm');
const span = document.createElement ('span');



password.addEventListener ('click', onClickHandler);

    function onClickHandler (){
    if (password.getAttribute('type') === 'password') {
        password.setAttribute('type', 'text');
        faEyeSlash.classList.remove('no-active');

    } else {
        password.setAttribute('type', 'password');
        faEyeSlash.classList.add('no-active');
    }
}



confirmPassword.addEventListener ('click', onClickHandlerConfirm);

    function onClickHandlerConfirm(){
    if(confirmPassword.getAttribute('type') === 'password') {
        confirmPassword.setAttribute('type', 'text');
        iconPasswordConfirm.classList.remove('confirm-no-active');
    }else{
        confirmPassword.setAttribute('type', 'password');
        iconPasswordConfirm.classList.add('confirm-no-active');
    }
}

btn.addEventListener('click', onClickHandlerBtn);

    function onClickHandlerBtn(){
    if(password.value === confirmPassword.value){
        alert("You are welcome!");
    } else {
        span.classList.add('span');
        span.innerText = "Нужно ввести одинаковые значения!";
        document.body.after(span);
        span.style.color = "red";
    }
}