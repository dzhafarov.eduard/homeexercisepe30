"use strict";

const input = document.getElementById("input");
const span = document.createElement("span");
const btn = document.createElement("button");

input.addEventListener("focus", inputFocusHandler);
input.addEventListener("blur", inputBlurHandler);


function inputFocusHandler(){
    input.style.outline = "none";
    input.style.border = "2px solid green";

}

function inputBlurHandler(){
    if(input.value < 0){
        input.style.border = "2px solid red";
        span.innerText = "Please enter correct price";
        document.body.after(span);
    }
    else {
        input.style.border = "1px solid black";
        input.style.color = "green";
        span.classList.add("span");
        document.body.before(span);
        span.innerText = `Текущая цена: ${input.value}`;
        btn.classList.add("button");
        document.body.before(btn);
        btn.innerText = "X";
    }


}

btn.addEventListener("click", onClickHandler);

function onClickHandler(){
    console.log("eduard");
    span.remove();
    btn.remove();
    input.value = "";
}