"use strict"

let picSlides = document.querySelectorAll(".image-to-show");
let slide = 0;
let picSlidesIntrv;

function start(){
    picSlidesIntrv = setInterval(() => {
        picSlides[slide].className = "no-active";
        slide = (slide + 1) % picSlides.length;
        picSlides[slide].className = "image-to-show";
    }, 3000);
}

start();

let btnStop = document.getElementById("stop");
let btnStart = document.getElementById("start");

btnStop.addEventListener("click", function(){
    clearInterval(picSlidesIntrv);
});

btnStart.addEventListener("click", function(){
    start();
})




