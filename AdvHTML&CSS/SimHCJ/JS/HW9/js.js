"use strict";

const tabsTitle = document.querySelectorAll(".tabs-title");
const tabsContentItem = document.querySelectorAll(".tabs-content-item");
const tabs = document.querySelector(".tabs");

tabs.addEventListener("click", onClickHandler);


function onClickHandler(event) {
    for(let i=0; i<tabsTitle.length; i++) {
        if (tabsTitle[i]===event.target){
            event.target.classList.add("active");
            tabsContentItem[i].classList.add("item-active");
        }else{
            tabsTitle[i].classList.remove("active");
            tabsContentItem[i].classList.remove("item-active");
        }
    }
}

