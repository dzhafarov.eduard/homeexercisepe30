"use strict";


const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let parent = document.body;
function getArr(arr, tag) {
    const ul = document.createElement("ul");
    ul.innerHTML = getList(arr);
    tag.append(ul);
}
 function getList(arr) {
    return arr.map((value) => {
            if (Array.isArray(value)) {
                return `<ul>${getList(value)}</ul>`;
            } else {
                return `<li>${value}</li>`;
            }
        })
         .join("");
 }

getArr(arr, parent);




