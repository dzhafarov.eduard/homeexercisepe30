let number1;
let number2;
let operator;


do{
    number1 = prompt("Insert number1: ");
}while(!number1 || isNaN(+number1) || number1.trim() === "");

do{
    number2 = prompt("Insert number2: ");
}while(!number2 || isNaN(+number2) || number2.trim() === "");

do{
    operator = prompt("Enter operator: +, - , *, /");
}while(
    operator !== "+" && operator !== "-" && operator !== "*" && operator !== "/");

makeOperations(number1, number2, operator);

function makeOperations(a, b, c){
    switch (c) {
        case "+":
            console.log( +a + +b);
            break;
        case "-":
            console.log( a - b);
            break;
        case "/":
            console.log( a / b);
            break;
        case "*":
            console.log( a * b);
            break;
    }
}