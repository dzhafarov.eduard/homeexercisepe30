"use strict";


const itemArr = document.querySelectorAll(".item-title");
const contentArr = document.querySelectorAll(".items-content-os");
document.querySelector(".item").addEventListener("click", onClickHandler);

function onClickHandler(event) {

    for(let i=0; i<itemArr.length; i++) {
        if (itemArr[i]===event.target){
            event.target.classList.add("active");
            contentArr[i].classList.add("os-content-active");
        }else{
            itemArr[i].classList.remove("active");
            contentArr[i].classList.remove("os-content-active");
        }
    }
}



let counter = 12;
let tabsItem = document.querySelectorAll(".picture-group li");
let btn = document.querySelector('.btn-load');
btn.addEventListener('click', function() {
    counter += 12;
    setTimeout(()=>{
        if (counter ===24){
            btn.classList.add('hide');
        }
        showItemsByData(all);
    }, 1000)
})

function showItemsByData(item){
    for(let i = 0; i < counter; i++){
        if (tabsItem[i].dataset.tab === item || item==='all'){
            tabsItem[i].classList.remove('hide')
        }else{
            tabsItem[i].classList.add('hide')
        }

    }
}
const tabsBtn = document.querySelector('.amazing-tabs');
let all = 'all';
tabsBtn.addEventListener('click', function(event){
    if(event.target){
        all = event.target.dataset.tab;
        showItemsByData(all);
    }
})


$(document).ready(function(){
    $('.slider').slick({
        arrows:true,
        dots:false,
        slidesToShow:(4),
        slidesToScroll: 1,
        focusOnSelect: true,
        speed:1000,
        infinite: true,
        asNavFor:'.sliderbig',
    });
    $('.sliderbig').slick({
        arrows:false,
        fade:true,
        asNavFor:'.slider',
    })
});



