"use strict";

document.getElementById('click').onclick = async function () {

    let {ip} = await (await fetch('https://api.ipify.org/?format=json')).json();
    console.log(ip);

    let data = await (await fetch(`https://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district,zip,lat,lon,timezone`)).json();
    console.log(data);

}
