const {src, dest} = require("gulp");
const imagemin  = require("gulp-imagemin");


const images = () =>{
    return src("./src/images/**/*.{png, jpg, jpeg, gif, svg}")
        .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        interlaced: true,
        optimizationLevel: 3
    })
        )
        .pipe(dest("./dist/images"))

}

exports.images = images;