const {src, dest} = require("gulp");
const sass= require("gulp-sass");
const browserSync = require("browser-sync");
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const minifyCss = require("gulp-cssnano");
const cleanCss = require("gulp-clean-css");


const style = () =>{
    return src("./src/scss/*.scss")
        .pipe(sass())
        .pipe(sass().on("error", sass.logError))
        .pipe(autoprefixer(['last 15 versions'], {cascade: true}))
        .pipe(minifyCss())
        .pipe(concat("styles.min.css"))
        .pipe(cleanCss({compatibility: 'ie8'}))
        .pipe(dest("./dist/css/"))
        .pipe(browserSync.stream());
};
exports.style = style;

